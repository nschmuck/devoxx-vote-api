-- add the columns
DROP MATERIALIZED VIEW toptalks;

CREATE MATERIALIZED VIEW toptalks AS
  SELECT vote.talkId as talkid,
         avg(vote.rating) as avg,
         count(vote.rating) as count,
         sum(vote.rating) as sum,
         talk.title as title,
         talk.type as type,
         talk.track as track,
         talk.summary as summary,
         talk.day as day,
         array_agg(distinct speaker.name) as speakers
  FROM unique_vote AS vote, talk, talk_by, speaker
  WHERE vote.talkId = talk.talkId AND talk_by.talkId = talk.talkId AND speaker.uuid = talk_by.speakerId
  GROUP BY talk.talkId, vote.talkId
  ORDER BY avg DESC;

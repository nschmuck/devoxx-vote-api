package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"regexp"
	"strings"
	"sync"
	"time"

	"bitbucket.org/jonmort/devoxx-vote-api/cfp"

	_ "github.com/lib/pq"
	"github.com/mattes/migrate/migrate"
)

var db *sql.DB
var votesChan = make(chan bool, 1)

const (
	topTalksDetails  = `SELECT talkId, avg, count, sum, title, type, track, summary, speakers, youtubeURL
                        FROM toptalks
                        WHERE count > 5 AND avg > 3 AND type != 'Keynote'`
	talkDetails      = `SELECT talkId, avg, count, sum, title, type, track, summary, speakers, youtubeURL
                        FROM toptalks
                        WHERE talkId = $1`
	votingOpen       = "SELECT true from talk WHERE starttime < now() AND talkId = $1"
	logVote          = "INSERT INTO vote (rating, talkId, userId, ip) VALUES ($1, $2, $3, $4)"
	speakerInsert    = `INSERT INTO SPEAKER (uuid, name)
                                SELECT $1, $2
                                WHERE NOT EXISTS (
                                      SELECT 1 FROM SPEAKER  where uuid = $1
                                )`
	talkInsert       = "INSERT INTO TALK (talkId, startTime, title, type, track, summary, day) VALUES ($1, to_timestamp($2), $3, $4, $5, $6, $7)"
	talkByInsert     = "INSERT INTO TALK_BY (talkId, speakerId) VALUES ($1, $2)"
	youtubeURLUpdate = "UPDATE talk SET youtubeURL = $1 WHERE talkId = $2"
	days             = "SELECT DISTINCT day from talk"
	tracks           = "SELECT DISTINCT track from talk"
	types            = "SELECT DISTINCT type from talk where type != 'Keynote'"
)

func InitDb(dbUrl string) error {
	var err error
	db, err = sql.Open("postgres", dbUrl)
	if err != nil {
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}
	errs, ok := migrate.UpSync(dbUrl, "./db")
	if !ok {
		log.Fatalf("Error migrating the database %v", errs)
		return errors.New(fmt.Sprintf("%v", errs))
	}
	go startUpdatingMaterializedView()
	return nil
}

// this function updates the materialized view when necessary
// where necessary is up to every 10 seconds, but only if a vote has been cast
func startUpdatingMaterializedView() {
	// for with no assignment will block and emit a value every 10s
	for range time.Tick(time.Second * 10) {
		// non blocking select to read from channel
		// this is idiomatic go
		select {
		case <-votesChan:
			log.Printf("Updating top talks")
			if _, err := db.Exec("REFRESH MATERIALIZED VIEW toptalks"); err != nil {
				log.Printf("Error occurred updating top talks: %v", err)
			}
		default:
		}
	}
}

// non blocking function to flag that there is an update pending
func setUpdateTopTalksNecessary() {
	// non blocking select to add a value to a channel
	// this is idiomatic go
	select {
	case votesChan <- true:
	default:
	}
}

// For each day of the conference, populate the db with data
func PopulateDataFromRest(applyOffset bool) error {
	// this is the time of the first talk on the wednesday
	offset := int64(0)
	if applyOffset {
		offset = 1434531600000 - (time.Now().Unix() * 1000)
	}

	conf := cfp.DevoxxUK2015()

	slots, err := conf.GetSlotsWithTalks()
	if err != nil {
		return err
	}
	var wg sync.WaitGroup

	wg.Add(len(slots))

	slotChan := make(chan cfp.ScheduleSlot, 10)

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	if _, err := tx.Exec("TRUNCATE TALK_BY CASCADE"); err != nil {
		return err
	}
	if _, err := tx.Exec("TRUNCATE TALK CASCADE"); err != nil {
		return err
	}
	if _, err := tx.Exec("TRUNCATE SPEAKER CASCADE"); err != nil {
		return err
	}

	for _, slot := range slots {
		log.Printf("%v", slot)
		go func(slot cfp.ScheduleSlot) {
			slotChan <- slot
		}(slot)
	}

	doneChan := make(chan bool)
	errorChan := make(chan error)

	go processAll(&wg, tx, offset, &conf, slotChan, doneChan, errorChan)

	errs := make([]error, 0)

	go func(errorChan <-chan error) {
		for {

			err, ok := <-errorChan
			if !ok {
				return
			}
			log.Printf("ERROR: %v, ", err)
			errs = append(errs, err)
		}
	}(errorChan)

	wg.Wait()
	log.Printf("DONE!!!")
	doneChan <- true
	close(errorChan)
	if len(errs) > 0 {
		if err := tx.Rollback(); err != nil {
			log.Printf("Rolling Back")
			errs = append(errs, err)
		}
	} else {
		if err := tx.Commit(); err != nil {
			return err
		}
	}
	setUpdateTopTalksNecessary()
	return errors.New(fmt.Sprintf("%v", errs))
}

func processAll(wg *sync.WaitGroup, tx *sql.Tx, offset int64, conf *cfp.Conference, slotChan chan cfp.ScheduleSlot, doneChan chan bool, errorChan chan error) {
	for {
		select {
		case slot := <-slotChan:
			// write talk
			if _, err := tx.Exec(talkInsert,
				slot.Talk.Id,
				int64((int64(slot.FromTimeMillis)-offset)/1000),
				slot.Talk.Title,
				slot.Talk.Type,
				slot.Talk.Track,
				slot.Talk.Summary,
				slot.Day); err != nil {
				log.Printf("failed to insert talk in slot: %v", slot)
				errorChan <- err
			}

			talk, err := conf.GetTalk(slot.Talk.Id)
			if err != nil {
				log.Printf("Failed to get Speakers for talk: %v", slot.Talk)
				errorChan <- err
			} else {
				speakers := talk.Speakers
				for _, speaker := range speakers {
					uuid := speaker.GetUuid()
					if _, err := tx.Exec(speakerInsert, uuid, speaker.Name); err != nil {
						errorChan <- err
					}
					if _, err := tx.Exec(talkByInsert, slot.Talk.Id, uuid); err != nil {
						log.Printf("speaker %v %v %v %v", speaker, uuid, slot, err)
						errorChan <- err
					}

				}
			}
			wg.Done()
		case <-doneChan:
			return
		}
	}
}

func LogVote(vote Vote, ip string) error {
	_, err := db.Exec(logVote,
		vote.Rating,
		vote.TalkId,
		vote.User,
		ip,
	)
	setUpdateTopTalksNecessary()
	return err
}

func IsVotingOpen(talkId string) (bool, error) {
	var started bool
	err := db.QueryRow(votingOpen, talkId).Scan(&started)
	if err == sql.ErrNoRows {
		return false, nil
	}
	return started, err
}

type TopXQuery func(int64) ([]interface{}, error)

func TopTalksQuery(limit int64, day string, talkType string, track string) ([]interface{}, error) {
	return topQuery(limit, topTalksDetails, day, talkType, track)
}

func TalkQuery(talkId string) (interface{}, error) {
	rows, err := db.Query(talkDetails, talkId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var talkId string
		var avg float64
		var count int
		var sum int
		var title string
		var talkType string
		var track string
		var summary string
		var speakers StringSlice
		var youtubeURL string
		if err := rows.Scan(&talkId, &avg, &count, &sum,
			&title, &talkType, &track, &summary, &speakers, &youtubeURL); err != nil {
			return nil, err
		}
		return map[string]interface{}{
			"name":       talkId,
			"avg":        fmt.Sprintf("%f", avg),
			"count":      fmt.Sprintf("%d", count),
			"sum":        fmt.Sprintf("%d", sum),
			"title":      title,
			"type":       talkType,
			"track":      track,
			"summary":    summary,
			"speakers":   speakers,
			"youtubeURL": youtubeURL,
		}, nil
	}
	return nil, err
}

func DaysQuery() ([]interface{}, error) {
	return categoryQuery(days)
}

func TypesQuery() ([]interface{}, error) {
	return categoryQuery(types)
}

func TracksQuery() ([]interface{}, error) {
	return categoryQuery(tracks)
}

func topQuery(limit int64, query string, day string, talkType string, tracks string) ([]interface{}, error) {

	if day != "" {
		query += " AND day = $2 "
	} else {
		query += " AND $2 = $2 "
	}

	if talkType != "" {
		query += " AND type = $3 "
	} else {
		query += " AND $3 = $3 "
	}

	if tracks != "" {
		query += " AND track = $4 "
	} else {
		query += " AND $4 = $4"
	}
	rows, err := db.Query(query+` LIMIT $1`, limit, day, talkType, tracks)

	if err != nil {
		return nil, err
	}
	order := make([]interface{}, 0)
	defer rows.Close()
	for rows.Next() {
		var talkId string
		var avg float64
		var count int
		var sum int
		var title string
		var talkType string
		var track string
		var summary string
		var speakers StringSlice
		var youtubeURL string
		if err := rows.Scan(&talkId, &avg, &count, &sum,
			&title, &talkType, &track, &summary, &speakers, &youtubeURL); err != nil {
			return nil, err
		}
		order = append(order, map[string]interface{}{
			"name":       talkId,
			"avg":        fmt.Sprintf("%f", avg),
			"count":      fmt.Sprintf("%d", count),
			"sum":        fmt.Sprintf("%d", sum),
			"title":      title,
			"type":       talkType,
			"track":      track,
			"summary":    summary,
			"speakers":   speakers,
			"youtubeURL": youtubeURL,
		})
	}
	return order, nil
}

func categoryQuery(query string) ([]interface{}, error) {

	rows, err := db.Query(query)

	if err != nil {
		return nil, err
	}
	category := make([]interface{}, 0)
	defer rows.Close()
	for rows.Next() {
		var cat string
		if err := rows.Scan(&cat); err != nil {
			return nil, err
		}
		category = append(category, cat)
	}
	return category, nil
}

func setYoutubeURL(talkId string, youtubeURL string) (sql.Result, error) {
	result, err := db.Exec(youtubeURLUpdate,
		youtubeURL,
		talkId,
	)
	return result, err
}

// the following code is taken from https://gist.github.com/adharris/4163702
// it is amazing
// it adds string array support to the database for postgres.

type StringSlice []string

// Implements sql.Scanner for the String slice type
// Scanners take the database value (in this case as a byte slice)
// and sets the value of the type.  Here we cast to a string and
// do a regexp based parse
func (s *StringSlice) Scan(src interface{}) error {
	asBytes, ok := src.([]byte)
	if !ok {
		return error(errors.New("Scan source was not []bytes"))
	}

	asString := string(asBytes)
	parsed := parseArray(asString)
	(*s) = StringSlice(parsed)

	return nil
}

// PARSING ARRAYS
// SEE http://www.postgresql.org/docs/9.1/static/arrays.html#ARRAYS-IO
// Arrays are output within {} and a delimiter, which is a comma for most
// postgres types (; for box)
//
// Individual values are surrounded by quotes:
// The array output routine will put double quotes around element values if
// they are empty strings, contain curly braces, delimiter characters,
// double quotes, backslashes, or white space, or match the word NULL.
// Double quotes and backslashes embedded in element values will be
// backslash-escaped. For numeric data types it is safe to assume that double
// quotes will never appear, but for textual data types one should be prepared
// to cope with either the presence or absence of quotes.

// construct a regexp to extract values:
var (
	// unquoted array values must not contain: (" , \ { } whitespace NULL)
	// and must be at least one char
	unquotedChar  = `[^",\\{}\s(NULL)]`
	unquotedValue = fmt.Sprintf("(%s)+", unquotedChar)

	// quoted array values are surrounded by double quotes, can be any
	// character except " or \, which must be backslash escaped:
	quotedChar  = `[^"\\]|\\"|\\\\`
	quotedValue = fmt.Sprintf("\"(%s)*\"", quotedChar)

	// an array value may be either quoted or unquoted:
	arrayValue = fmt.Sprintf("(?P<value>(%s|%s))", unquotedValue, quotedValue)

	// Array values are separated with a comma IF there is more than one value:
	arrayExp = regexp.MustCompile(fmt.Sprintf("((%s)(,)?)", arrayValue))

	valueIndex int
)

// Find the index of the 'value' named expression
func init() {
	for i, subexp := range arrayExp.SubexpNames() {
		if subexp == "value" {
			valueIndex = i
			break
		}
	}
}

// Parse the output string from the array type.
// Regex used: (((?P<value>(([^",\\{}\s(NULL)])+|"([^"\\]|\\"|\\\\)*")))(,)?)
func parseArray(array string) []string {
	results := make([]string, 0)
	matches := arrayExp.FindAllStringSubmatch(array, -1)
	for _, match := range matches {
		s := match[valueIndex]
		// the string _might_ be wrapped in quotes, so trim them:
		s = strings.Trim(s, "\"")
		results = append(results, s)
	}
	return results
}

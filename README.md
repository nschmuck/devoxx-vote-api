# Developer Notes

This code is written in golang. The REST API is built using gin which appears to
be quite a popular Go framework for REST and HTTP based APIs. When searching for
information it is worth using the phrase golang instead of go. The API itself is
very simple but it does require a Postgres database. The simplest way to get Postgres
working is to use Docker to run a Postgres container.

On OSX with brew run: (on other systems install docker and boot2docker if necessary)
* `brew install docker boot2docker`

then run `boot2docker init` followed by `eval $(boot2docker shellinit)` which
will setup your docker environment with boot2docker. Boot2docker will be working fine if you receive a valid ip after running `boot2docker ip`. In case it hasn't been started, you can run `boot2docker up`.

Launch a Postgres server using: `docker run --name postgres-db -p 5432:5432 -d postgres`

Connect to the CLI using:
`docker exec -it postgres-db /usr/lib/postgresql/9.4/bin/psql -U postgres`

Remember that to use docker with boot2docker it is necessary to use
`eval $(boot2docker shellinit)` to ensure that your environment is set correctly.

You will also need a working copy of go to build and run the code. Install using
brew `brew install go`
After installing go, the GOPATH environment variable will need to be configured (https://golang.org/doc/code.html#GOPATH).

## Code

The main entry point to the code is the main method in `devoxx-vote-api.go`.

To build the code use `go build`. If the command doesn't find some packages, you can download them using `go get PACKAGE_URL`

To prepare the database use: `./devoxx-vote-api --reload=true --dbUrl="postgres://postgres@$(boot2docker ip):5432/postgres?sslmode=disable"`

To run use `./devoxx-vote-api --bind="localhost:8080" --reload=false --requireOpen=false --applyOffset=true --dbUrl="postgres://postgres@$(boot2docker ip):5432/postgres?sslmode=disable"`

A better workflow is to use go watcher. Install using `go get github.com/canthefason/go-watcher`
And then run using `watcher --redisUrl="redis://$(boot2docker ip):6379" --reload=false --requireOpen=false
--applyOffset=true --bind='127.0.0.1:8081'` this will reload and compile the application on any changes
and the arguments will prevent the data from being loaded from the Devoxx REST
API more than once

## Dependencies

It is necessary to deploy the dependencies with the code for Heroku. This means
using godep which can be installed using ` go get github.com/kr/godep`. Then
after changing a dependency run `godep get; godep save`. Commit the results.


## Heroku Deployment

You'll need a Heroku account. Sign up if you don't have one.

First install the heroku toolbelt `brew install heroku-toolbelt` and then login `heroku login`

Once you're all set up with heroku you can add a remote to deploy to `heroku  git:remote -a devoxxvoting`
This will have added a remote called `heroku` to your git remotes. See `git remote -v`

The go build pack is needed. Add that to the project using `heroku buildpacks:add https://github.com/heroku/heroku-buildpack-go.git`
This is a one time operation and can only be done by an owner.

You're all ready to deploy now. Simply do a push `git push heroku master` and the app will deploy.

To refresh the talk data use `heroku run populatedb` (this is defined in the `Procfile`)

A Postgres addon is required for any of this to work.

Check the logs using `heroku logs -t`

# REST API

FORMAT: 1A
HOST: https://evening-mesa-4747.herokuapp.com/

# Devoxx BE 2015 Vote API
The Devoxx BE 2015 Vote API is a service that allows Devoxxians to vote on the talks.
A vote can be cast a talk has started.
Votes are:
* Tied to user id which comes from Eventbrite and is an integer
* From 1-5:
 * 5: Awesome
 * 4: Good
 * 3: OK
 * 2: Meh
 * 1: Terrible


# Vote [/DV15/vote]

## Submit a vote [POST]

Vote on a talk. Vote is from 1-5 and is made on a talk.

A vote has the following attributes:

+ user (number) - The EventBrite user id
+ rating (number)- A rating from 1-5
+ talkId (string) - A talk to vote on. This id is defined by http://cfp.devoxx.be/api/profile/talk

+ Request (application/json)

            {
                "rating": 5,
                "talkId": "MBG-3462",
                "user": 10
            }


+ Response 201 (application/json)

    + Body

            {
                "rating": 5,
                "talkId": "MBG-3462",
                "user": 10
            }

+ Response 500 (application/json)

    + Body

            {
                "message": "Cannot vote on talk yet"
            }


+ Response 409 (application/json)

    + Body

            {
                "message": "Cannot vote more than once"
            }



# Top Talks [/DV15/top/talks{?limit}&{day}&{talkType}&{track}]

## Talks [GET]

Retrieve the top talks as voted on. Talks are sorted by average rating.
Each talk has the following attributes:

+ avg (number) - The average rating for the talk (sum/count)
+ count (number) - The total number of votes cast
+ sum (number) - The cumlative rating
+ name (string) - The talk ID

+ Parameters

    + limit (number, optional) - The maximum number of results to return. 1-100
        + Default: `10`
    + day (string, optional) - The day to filter by (monday, tuesday, wednesday, thursday, friday)
        + Default: empty
   + talkType (string, optiona) - Talk type to filter by
        + Default: empty
   + track (string, optional) - Track to filter by
        + Default: empty

+ Response 200 (application/json)

    + Body

            {
               "talks" : [
                  {
                     "avg" : "5",
                     "count" : "2",
                     "sum" : "10",
                     "name" : "VML-9224"
                  },
                  {
                     "count" : "85",
                     "avg" : "3.0588235294117645",
                     "sum" : "260",
                     "name" : "AOG-3279"
                  },
                  {
                     "count" : "88",
                     "avg" : "3.0454545454545454",
                     "name" : "YIW-0702",
                     "sum" : "268"
                  },
                  {
                     "name" : "CKD-6073",
                     "sum" : "240",
                     "avg" : "3",
                     "count" : "80"
                  },
                  {
                     "name" : "MBG-3462",
                     "sum" : "288",
                     "count" : "100",
                     "avg" : "2.8799999999999999"
                  },
                  {
                     "count" : "100",
                     "avg" : "2.77",
                     "name" : "DIU-5372",
                     "sum" : "277"
                  },
                  {
                     "count" : "4",
                     "avg" : "2",
                     "sum" : "8",
                     "name" : "DNL-9930"
                  }
               ]
            }

# Talk [/DV15/talk/:talkId]

## Categories [GET]

Retrieve the details of a specific talk


+ Response 200 (application/json)

    + Body

            {
               "sum" : "5",
               "count" : "1",
               "title" : "Apache Sling as an OSGi-powered REST middleware",
               "summary" : "<p>Apache Sling is an innovative web ....",
               "avg" : "5.000000",
               "name" : "RRH-9866",
               "type" : "Tools-in-Action",
               "track" : "Server Side Java",
               "speakers" : [
                  "Robert Munteanu"
               ]
            }


# Categories [/DV15/categories]

## Categories [GET]

Retrieve the categories that can be filtered by. day, track and talkType can all
be passed to top/talks fo filter the top talks. Each of the categories can be used
as a filter value.


+ Response 200 (application/json)

    + Body

            {
               "tracks" : [
                  "Server Side Java",
                  "Startups",
                  "Java SE",
                  "Architecture & Security",
                  "Methodology & DevOps",
                  "JVM Languages",
                  "Future<Devoxx>",
                  "Mobile",
                  "Cloud & BigData",
                  "Web & HTML5"
               ],
               "days" : [
                  "friday",
                  "thursday",
                  "tuesday",
                  "monday",
                  "wednesday"
               ],
               "talkTypes" : [
                  "Tools-in-Action",
                  "Hand's on Labs",
                  "BOF (Bird of a Feather)",
                  "Quickie",
                  "Ignite Sessions",
                  "Conference",
                  "University",
                  "Startup presentation"
               ]
            }
